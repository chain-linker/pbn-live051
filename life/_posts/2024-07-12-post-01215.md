---
layout: post
title: "[분양정보] 롯데캐슬 위너스포레 입주자 모집공고"
toc: true
---


 

 

 안녕하세요. "부동산 시작하는 김씨"입니다.
 2024년 6월 14일 청약Home APT 분양정보 중에서 "롯데캐슬 위너스포레 입주자 모집공고"에 대해서 소개해 드리도록 하겠습니다.

## ◑ 롯데캐슬 위너스포레 입주자 모집공고
 

 ※ 단지 주요정보

 

 ◎ 신청자격 및 전매제한

 

 ■ 신청자격은 당첨자를 대상으로 전산조회, 제출서류 등을 통해 사업주체에서 확인하며, 확인결과 신청자격과 다르게 당첨된 사실이 판명될 경우에는 부적격 당첨자로서 불이익(계약체결 불가, 일정기간 입주자저축 상용 및 입주자선정 한계 등)을 받으니 기어이 입주자모집공고문의 신청자격, 기준, 일정, 방법, 유의사항 등을 이어서 확인 내종 신청하시기 바랍니다.
 ■ 결부 주택건설지역(경기도 오산시)은 「주택법」 제63조의 2에 의한 비투기과열지구 및 비청약과열지역으로서, 본 아파트는 「주택공급에 관한 규칙」에 따라 1주택 야망 소유한 세대에 속한 분도 1순위 자격이 부여됩니다.

 

 ※ 단, 기관추천 특별공급 반중간 장애인, 국가유공자 및 철거주택 소유자(도시재생 부지제공자)는 청약통장 불필요
 ※ 1순위 : 입주자저축에 가입하여 가입기간이 12개월이 경과하고 지역별·면적별 예치금액 마지막 납입한 분
 ※ 2순위 : 입주자저축에 가입하였으나, 1순위에 해당되지 않는 분
 

 

 ■ 본 주택의 전매제한은 애초 당첨자발표일로부터 적용되며 기간은 아래와 같습니다.
 

 ◎ 청약 및 뇌약 등 주요일정

 ※ 롯데캐슬 위너스포레 견본주택 : 경기도 오산시 원동 173-1번지
 

 

 ◎ 공급대상 및 공급금액

 

 ■ 공급위치 : 경기도 오산시 양산동 95번지 일원

 ■ 공급규모 : 아파트 지하 2층, 적괴 27층 16개동 총 1672세대 한복판 일반분양 834세대
      [특별공급 327세대(기관추천 60세대, 다자녀가구 81세대, 신혼부부 109세대, 노부모부양 23세대, 생애최초 54세대) 포함] 및 부대복리시설
 ■ 입주시기 : 2027년 8월 예정(정확한 입주일자는 추후 통보함)
 ■ 공급대상  (단위 : ㎡, 세대)

 

 ■ 공급금액 및 납부일정  (단위 : 세대, 원)

 

 ■ 본 아파트는 전세대 발코니 확장을 무상으로 합 시공하여 공급하오니 청약 및 입약 전 이를 기어이 확인하기 바라며, 향후 발코니 미확장 요청 및 미확장에 따른 공사비 금액을 사업주체에 요구할 생명 없습니다.

 

 

 ◎ 기타사항
 ■ 사업주체(시행사) : 스마트시티오산 지역주택조합 / 시공회사 : 롯데건설(주)

 ■ 견본주택 : 경기도 오산시 원동 173-1번지

 ■ 사업지 : 경기도 오산시 양산동 95번지 일원
 ■ 홈페이지 : [오산롯데캐슬 위너스포레](https://hinderpeaceful.com/life/post-00108.html) https://www.lottecastle.co.kr

 ■ 분양문의 : 1551-1672
 

### ◑ 롯데캐슬 위너스포레

 ◎ 롯데캐슬 위너스포레 사업개요

 

 ◎ 롯데캐슬 위너스포레 입지환경

 

 

 

 ◎ 롯데캐슬 위너스포레 프리미엄

 

 

 

 여타 자세한 사항은 입주자 모집공고문을 꼼꼼하게 확인하시기 바랍니다.

 

 

 ※ 첨부자료

 

 

 ※ 출처. 청약Home 청약일정 및 통계

 

